IDOUX Paul – LEJOSNE Charles mars 2021

IMDB SEARCH

1. **Description générale du projet**

L&#39;objectif du projet était de développer une application java sous Android Studio. Nous avons donc décider de réaliser une application de recherche de critiques de films. Une maquette a été réalisé sous Adobe XD afin de définir les zones et les composants de l&#39;interface utilisateur.

![](https://i.imgur.com/zozIAwt.png)

Le premier layout comprend simplement une barre de recherche. En effectuant la recherche, l&#39;objectif est d&#39;afficher la liste des résultats en utilisant une recycler view. En cliquant sur la « box » d&#39;un film, cela affiche une page avec les détails et différentes critiques du film.

2. **Statut du projet**

La fonction principale du projet, la recherche et l&#39;affichage de la liste des résultats sont terminés (algorithme + graphisme ). La recherche s&#39;effectue grâce à une API permettant de récupérer une liste en fonction du titre du film demandé.

![](https://i.imgur.com/6oJ6EkM.png)
 ![](https://i.imgur.com/Yyq7jZK.png)
 ![](https://i.imgur.com/8Gg1Rjt.png)

Une deuxième API est utilisée afin de récupérer les détails d&#39;un film comme le titre, la date de sortie, les acteurs, le réalisateur, le résumé, etc…

Les liens des API sont les suivants :

3. **Instruction d&#39;installation et d&#39;utilisation**

 => Ouvrir le zip sous Android Studio. Le projet tourne sous Android 11 et API 30, adapter la configuration.
 
 => Fichiers Activities sous [main \ java \ com.example.imdbsearch]

4. **Problèmes connus**

Les API utilisées (gratuites) ont une limite de 500 requêtes par mois. L&#39;utilisation excessive de l&#39;application peut donc entrainer un arrêt des APIs.

5. **Améliorations possibles**

Auto-Complétion

Intégration d&#39;un arrière-plan vidéo

Écran d&#39;accueil (suggestions de films, dernières sorties, meilleurs critiques, etc…)
