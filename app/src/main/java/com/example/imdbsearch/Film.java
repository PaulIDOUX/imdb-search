package com.example.imdbsearch;

public class Film {
    private String id;
    private String name;
    private String url;
    private String year;
    private String actors;
    private String director;
    private String ratingImdb;
    private String ratingRotten;
    private String rattingMeta;
    private String award;
    private String boxOffice;
    private String plot;
    private String runTime;
    private String genre;


    public Film(String id, String name, String url, String year) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.year = year;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public String getYear() {
        return year;
    }

    public void updateFilm(String actors, String director, String ratingImdb, String ratingRotten, String rattingMeta, String award, String boxOffice, String plot, String runTime, String genre) {
    this.actors = actors;
    this.director = director;
    this.ratingImdb = ratingImdb;
    this.ratingRotten = ratingRotten;
    this.rattingMeta = rattingMeta;
    this.award = award;
    this.boxOffice = boxOffice;
    this.plot = plot;
    this.runTime = runTime;
    this.genre = genre;
    }
    public String getActors() {
        return actors;
    }

    public String getAward() {
        return award;
    }

    public String getBoxOffice() {
        return boxOffice;
    }

    public String getDirector() {
        return director;
    }

    public String getGenre() {
        return genre;
    }

    public String getPlot() {
        return plot;
    }

    public String getRatingImdb() {
        return ratingImdb;
    }

    public String getRatingRotten() {
        return ratingRotten;
    }

    public String getRattingMeta() {
        return rattingMeta;
    }

    public String getRunTime() {
        return runTime;
    }
}
