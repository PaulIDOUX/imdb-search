package com.example.imdbsearch;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.squareup.picasso.Picasso;

public class FilmActivity extends AppCompatActivity{
    Film film;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);
        film = Singleton.getInstance().getCurrentFilm();
        ImageView cover = findViewById(R.id.item_movie_url);
        ImageView IMDBcover = findViewById(R.id.item_movie_imdbCover);
        ImageView ROTTENcover = findViewById(R.id.item_movie_RottenCover);
        ImageView METAcover = findViewById(R.id.item_movie_MetaCover);

        TextView name = findViewById(R.id.item_movie_name);
        TextView actors = findViewById(R.id.item_movie_actors);
        TextView director = findViewById(R.id.item_movie_director);
        //TextView runtime = findViewById(R.id.textview_restaurant_name);
        TextView imdbR = findViewById(R.id.item_movie_ratingImdb);
        TextView rottenR = findViewById(R.id.item_movie_ratingRotten);
        TextView metaR = findViewById(R.id.item_movie_ratingMeta);
        TextView year = findViewById(R.id.item_movie_year);
        //TextView boxoffice = findViewById(R.id.textview_restaurant_name);
        //TextView genre = findViewById(R.id.textview_restaurant_name);
        //TextView award = findViewById(R.id.textview_restaurant_name);
        TextView plot = findViewById(R.id.item_movie_plot);
        Picasso.get().load(film.getUrl()).into(cover);
        Picasso.get().load("https://m.media-amazon.com/images/G/01/imdb/images/social/imdb_logo._CB410901634_.png").into(IMDBcover);
        Picasso.get().load("https://i1.sndcdn.com/avatars-000642091035-94htfd-t500x500.jpg").into(ROTTENcover);
        Picasso.get().load("https://pbs.twimg.com/profile_images/527528131171590144/EQXs3lpX_400x400.png").into(METAcover);
        name.setText(film.getName());
        //commit
        actors.setText(film.getActors());
        director.setText(film.getDirector());
        plot.setText(film.getPlot());
        imdbR.setText(film.getRatingImdb());
        year.setText(film.getYear());
        rottenR.setText(film.getRatingRotten());
        metaR.setText(film.getRattingMeta());

    }
}


