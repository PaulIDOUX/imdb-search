package com.example.imdbsearch;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.ArrayMap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mancj.materialsearchbar.MaterialSearchBar;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    private MaterialSearchBar searchBar;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private OkHttpClient client = new OkHttpClient();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy().Builder().permitAll().build();
       // StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.movies_list);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new MyAdapter();
        recyclerView.setAdapter(mAdapter);

        searchBar = (MaterialSearchBar) findViewById(R.id.searchBar);
        searchBar.setHint("Rechercher un film");
        searchBar.setSpeechMode(false);
        //enable searchbar callbacks


        searchBar.setOnSearchActionListener(new MaterialSearchBar.OnSearchActionListener() {
            @Override
            public void onSearchStateChanged(boolean b) {
            }



            @Override
            public void onSearchConfirmed(CharSequence charSequence) {

                HttpUrl.Builder urlBuilder = HttpUrl.parse("https://imdb8.p.rapidapi.com/auto-complete").newBuilder();
                urlBuilder.addQueryParameter("q", charSequence.toString());
                String url = urlBuilder.build().toString();

                Request request = new Request.Builder().url(url)
                        .get()
                        .addHeader("x-rapidapi-key", "2d2e5f766cmshfa62f307aeb6476p1f5915jsn0f594805a357")
                        .addHeader("x-rapidapi-host", "imdb8.p.rapidapi.com")
                        .build();
                Call call = client.newCall(request);
                call.enqueue(new okhttp3.Callback() {
                    public void onResponse(Call call, Response response)
                            throws IOException {
                        try {
                            Singleton.getInstance().setFilmArrayList(response);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mAdapter.notifyDataSetChanged();
                                }
                            });

                        } catch (JSONException e) {
                        }
                    }

                    public void onFailure(Call call, IOException e) {
                    }
                });

                searchBar.closeSearch();
            }

            @Override
            public void onButtonClicked(int buttonCode) {

            }
        });
        //Inflate menu and setup OnMenuItemClickListener
        //searchBar.inflateMenu(R.menu.main);
        //searchBar.getMenu().setOnMenuItemClickListener(this);
    }

    public void goToFilmActivity(){
        Intent intent = new Intent(this, FilmActivity.class);
        startActivity(intent);
    }

    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>{

        @NonNull
        @Override
        public MyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(R.layout.item_movie2, parent, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            holder.setFilm(Singleton.getInstance().getFilmAtPosition(position));
        }

        @Override
        public int getItemCount() {
            return Singleton.getInstance().getNumberOfFilm();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView textview_movie_name;
            ImageView imageView_movie_logo;
            TextView yearview_movie_year;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                textview_movie_name = (TextView) itemView.findViewById(R.id.item_movie_title);
                imageView_movie_logo = (ImageView) itemView.findViewById(R.id.item_movie_poster);
                yearview_movie_year = (TextView) itemView.findViewById(R.id.item_movie_release_date);


            }

            public void setFilm(final Film film) {
                CardView cardView = this.itemView.findViewById(R.id.card_view);
                if (film.getId().startsWith("tt")) {
                    cardView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Singleton.getInstance().setCurrentFilm(film);
                            HttpUrl.Builder urlBuilder = HttpUrl.parse("https://www.omdbapi.com/").newBuilder();
                            urlBuilder.addQueryParameter("i", film.getId());
                            urlBuilder.addQueryParameter("apikey", "be19bf63");
                            String url = urlBuilder.build().toString();

                            Request request = new Request.Builder().url(url)
                                    .get()
                                    .build();
                            Call call = client.newCall(request);
                            call.enqueue(new okhttp3.Callback() {
                                public void onResponse(Call call, Response response)
                                        throws IOException {
                                    try {
                                        ArrayMap dataToComplete = Singleton.getInstance().getFilmDetail(response);
                                        film.updateFilm(dataToComplete.get("actor").toString(),dataToComplete.get("director").toString(), dataToComplete.get("Internet Movie Database").toString(),dataToComplete.get("Rotten Tomatoes").toString(), dataToComplete.get("Metacritic").toString(), dataToComplete.get("award").toString(), dataToComplete.get("boxoffice").toString(), dataToComplete.get("plot").toString(), dataToComplete.get("runtime").toString(), dataToComplete.get("genre").toString());
                                        goToFilmActivity();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }

                                public void onFailure(Call call, IOException e) {
                                }
                            });
                        }
                    });
                }

                textview_movie_name.setText(""+film.getName());
                yearview_movie_year.setText(""+film.getYear());
                Picasso.get().load(film.getUrl())
                        .into(imageView_movie_logo, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError(Exception e) {
                            }
                        });

            }
        }
    }
}