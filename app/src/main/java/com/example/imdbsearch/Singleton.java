package com.example.imdbsearch;


import android.util.ArrayMap;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

class Singleton {
    private static final Singleton ourInstance = new Singleton();

    public static Singleton getInstance() {
        return ourInstance;
    }


    private Film currentFilm;

    private ArrayList<Film> filmArrayList;

    private Singleton() {
        this.filmArrayList = new ArrayList<>();
    }

    public void setFilmArrayList(Response response) throws IOException, JSONException {
       filmArrayList = jsonToArray(response);
    }

    public ArrayMap getFilmDetail(Response response) throws IOException, JSONException {
        return jsonToData(response);
    }

    public void setCurrentFilm(Film film){
        currentFilm = film;
    }

    public Film getCurrentFilm(){
        return  currentFilm;
    }


    public Film getFilmAtPosition(int position){
        return filmArrayList.get(position);
    }

    public int getNumberOfFilm(){
        return filmArrayList.size();
    }

    private ArrayList<Film> jsonToArray (Response response) throws JSONException, IOException {
        String reponse = response.body().string();
        ArrayList<Film> arrayList = new ArrayList<>();
        JSONObject jsonReponse = new JSONObject(reponse);
        if(jsonReponse.has("d")){
            JSONArray jsonArray = jsonReponse.optJSONArray("d");
            for (int i=0 ; i < jsonArray.length(); i++){
                JSONObject item = jsonArray.getJSONObject(i);
                String id = item.getString("id");
                String name = item.getString("l");
                String urlImg = "https://wolvesweekly.files.wordpress.com/2020/03/stock-vector-online-cinema-art-movie-watching-with-popcorn-and-film-strip-cinematograph-concept-vintage-retro-584655766.jpg";
                if(item.has("i")){
                    urlImg = item.getJSONObject("i").getString("imageUrl");
                }
                String year = "";
                if(item.has("y")){
                    year = item.getString("y");
                }




                Film film = new Film(id,name,urlImg, year);
                arrayList.add(film);
            }
        }

        return arrayList;
    }
    private ArrayMap jsonToData (Response response) throws IOException, JSONException {
        ArrayMap data = new ArrayMap();
        String reponse = response.body().string();
        JSONObject jsonReponse = new JSONObject(reponse);
        String runtime = (!jsonReponse.getString("Runtime").equals("N/A")) ? jsonReponse.getString("Runtime"): "No data";
        data.put("runtime", runtime);
        String genre = (!jsonReponse.getString("Genre").equals("N/A")) ? jsonReponse.getString("Genre"): "No data";
        data.put("genre", genre);
        String director = (!jsonReponse.getString("Director").equals("N/A")) ? jsonReponse.getString("Director"): "No data";
        data.put("director", director);
        String actor = (!jsonReponse.getString("Actors").equals("N/A")) ? jsonReponse.getString("Actors"): "No data";
        data.put("actor", actor);
        String plot = (!jsonReponse.getString("Plot").equals("N/A")) ? jsonReponse.getString("Plot"): "No data";
        data.put("plot", plot);
        String award = (!jsonReponse.getString("Awards").equals("N/A")) ? jsonReponse.getString("Awards"): "No data";
        data.put("award", award);
        String boxoffice = (!jsonReponse.getString("BoxOffice").equals("N/A")) ? jsonReponse.getString("BoxOffice") : "No data";
        data.put("boxoffice", boxoffice);
        data.put("Internet Movie Database", "No data");
        data.put("Rotten Tomatoes", "No data");
        data.put("Metacritic","No data");
        if (jsonReponse.has("Ratings") && jsonReponse.optJSONArray("Ratings").length() > 0) {
            JSONArray rating = jsonReponse.optJSONArray("Ratings");
            for (int i=0 ; i < rating.length(); i++) {
                JSONObject item = rating.getJSONObject(i);
                data.put(item.getString("Source"),item.getString("Value"));
            }
        }

        return data;
    }


}
